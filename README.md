# Developing for Codeberg

Step 1: Clone this repository:
```
git clone --recurse-submodules https://codeberg.org/momar/codeberg-deployment.git && cd deployment
```

Step 2: Start it with Docker Compose:
```
docker-compose -p codeberg up --build --force-recreate
```

You can now access your very own Codeberg test instance at http://localhost.

To clear all data stored in the instance, run `docker-compose -p codeberg down -v`
